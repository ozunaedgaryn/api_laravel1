<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\FormularioController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [UserController::class, 'register'])->name('register');
Route::post('login', [UserController::class, 'login'])->name('login');

Route::group( ['middleware' => ["auth:sanctum"]], function(){
    //Rutas
    Route::get('logout', [UserController::class, 'logout'])->name('logout');
    Route::get('user_profile', [UserController::class, 'user_profile'])->name('user_profile');
    Route::get('usuarios', [UserController::class, 'usuarios'])->name('usuarios');


    ////////////CRUD//////////////////////////////////////////////////////////////
    Route::post('crear_form', [FormularioController::class, 'crear_form'])->name('crear_form');
    Route::get('show_form', [FormularioController::class, 'show_form'])->name('show_form');

    Route::get('show_one_form/{id}', [FormularioController::class, 'show_one_form'])->name('show_one_form');

    Route::put('update_form/{id}', [FormularioController::class, 'update_form'])->name('update_form');
    Route::delete('delete_form/{id}', [FormularioController::class, 'delete_form'])->name('delete_form');
});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
