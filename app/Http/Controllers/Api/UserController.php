<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\HttpFoundation\Cookie;
use Cookie;
use App\Models\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DataTables;

class UserController extends Controller
{
    //

    public function register(Request $request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->fecha = date('Y-m-d');
        $user->save();

        // return response()->json([
        //     "status" => 1,
        //     "msg" => "Usuario Creado",
        // ]);
        return response($user, Response::HTTP_CREATED);

    }

    public function login(Request $request): JsonResponse
    {
        // $data = json_decode($request->getContent());
        // $user = User::where('name', '=', $data->name)->first();

        $user = User::where('name', '=', $request->name)->first();
        if(isset($user->id)){
            if(Hash::check($request->password, $user->password)){
                // Creamos el Token
                // $token = $user->createToken("auth_token")->plainTextToken;
                // return response()->json([
                //     "status" => 0,
                //     "msg" => "Usuario Logueado",
                //     "access_token" => $token
                // ]);

                 $token = $user->createToken("token")->plainTextToken;
                
                 // $cookie = cookie('cookie_token' , $token, 60 * 24);
                 
                 return response()->json([
                    'status' => true,
                    'msj' => 'Usuario Logueado',
                    'token'=> $token
                 ]);
                 // ->withoutCookie($cookie);

            }else{
                return response()->json([
                    "status" => false,
                    "msg" => "Clave Invalidad",
                ], Response::HTTP_UNAUTHORIZED);
                // return response(Response::HTTP_UNAUTHORIZED);
            }
        }else{
            return response()->json([
                "status" => false,
                "msg" => "Usuario no registrado",
            ], Response::HTTP_UNAUTHORIZED);
        }
    }

    public function user_profile(): JsonResponse
    {
        return response()->json([
            "status" => 1,
            "msg" => "logueado",
            "data" => Auth::user()
        ], Response::HTTP_OK);
    }

    public function logout(): JsonResponse
    {

        Auth::user()->tokens()->delete();
        // $cookie = Cookie::forget('cookie_token');
        return response()->json([
            'status'=>true,
            "msj"=>"Cierre de Sesion"
        ], Response::HTTP_OK);
        // ->withoutCookie($cookie);
    }

    public function usuarios()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $user = User::select('id', 'name', 'email')->get();
        // return response()->json(
        //     Datatables::of($user)->make(true)
        // , 200);
        return Datatables::of($user)->make(true);
    }

}
