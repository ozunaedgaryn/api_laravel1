<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Formulario;
use Illuminate\Support\Facades\Auth;


class FormularioController extends Controller
{
    public function crear_form(Request $request){
        $form = new Formulario();
        $form->id_user = Auth::user()->id;
        $form->color = $request->color;
        $form->deporte = $request->deporte;
        $form->fecha = date('Y-m-d');
        $form->save();

        return response()->json([
            "status" => 1,
            "msj" => "Formulario creado"
        ]);
    }

    public function show_form(){
        $forms = Formulario::where('id_user', '=', Auth::user()->id)->get();
        return response()->json([
            "status" => 1,
            "msj" => "Listados",
            "data" => $forms
        ]);
    }

    public function show_one_form($id){

    }

    public function update_form(Request $request, $id){
        if(Formulario::where(["id_user" => Auth::user()->id, "id"=>$id])->exists()){
            $form = Formulario::find($id);
            $form->color = isset($request->color) ? $request->color : $form->color;
            $form->deporte = isset($request->deporte) ? $request->deporte : $form->deporte;
            $form->save();
            return response()->json([
                "status" => 1,
                "msj" => "Formulario Actualizado"
            ]);
        }else{
            return response()->json([
                "status" => 0,
                "msj" => "Formulario no encontrado"
            ], 404);
        }
    }

    public function delete_form($id){
        if(Formulario::where(["id_user" => Auth::user()->id, "id"=>$id])->exists()){
            $form = Formulario::where(["id_user" => Auth::user()->id, "id"=>$id])->first();
            $form->delete();
            return response()->json([
                "status" => 1,
                "msj" => "Formulario Eliminado"
            ]);
        }else{
            return response()->json([
                "status" => 0,
                "msj" => "Formulario no encontrado"
            ], 404);
        }
    }
}
